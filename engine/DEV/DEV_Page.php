
<h1> Міграції </h1>



<?php

echo "<table border='1'>"; // start the table with a border

// Loop through the array
foreach (\LIB\LIB_SQL::Select('Migration') as $row) {
    echo "<tr>"; // start a new row
    foreach ($row as $cell) {
        echo "<td>" . htmlspecialchars($cell) . "</td>"; // create a cell for each item
    }
    echo "</tr>"; // end the row
}

echo "</table>"; // end the table
?>
<button onclick="url_jump('migration');">Робити міграцію</button><br>
<button onclick="url_jump('migration_reset');">Робити міграцію з перезапуском</button>

<script>
    var url_jump = function (url)
    {
        if (confirm('Ви впевнені? '+url)) {
            window.location.href = "<?php echo PROJECT_URL_FULL;?>"+"dev/"+url+"/?DEV="+"<?php echo DEV_KEY;?>";
        }
    }
</script>