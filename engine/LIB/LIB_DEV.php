<?php

namespace LIB;

class LIB_DEV
{
    public static function IsDev()
    {
        $IsDev=false;


        if(isset($_GET['DEV']))
        {

            if($_GET['DEV']==DEV_KEY)
            {
                $IsDev=true;
            }
        }
        return $IsDev;
    }

    public static function Site_status()
    {
        if(PROJECT_SITE_STATUS!='Active')
        {
            echo "Винекла технычна помилка - почекайте пару годин, ми чкоро повернемося =)";
            die();
        }
    }
}