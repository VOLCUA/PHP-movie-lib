<?php

namespace LIB;

class LIB_SQL
{
    static public function CheckConnect()
    {

        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS,DB_NAME,DB_PORT);

        if (!$conn) {
            $Connect = false;
            $Error = "Connection failed: " . mysqli_connect_error();
        } else {
            $Connect = true;
            $Error = "";
        }

        $result = [
            'connect' => $Connect,
            'error' => $Error
        ];

        if ($conn) {
            mysqli_close($conn);
        }


        if($result['connect']==false)
        {
            echo "Винекла технічна помилка - проблема зєднання з базою данних =)";
            if(\LIB\LIB_DEV::IsDev()){echo $result['error'];}
            die();
        }

        return $result;
    }

    static public function checkIfTableExists( $tableName) {

        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS,DB_NAME,DB_PORT);

        if (empty($tableName)) {
            return false;
        }

        $query = "SHOW TABLES LIKE '" . $conn->real_escape_string($tableName) . "'";
        $result = $conn->query($query);


        if ($conn) {
            mysqli_close($conn);
        }
        // Перевірка наявності таблиці
        if ($result && $result->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    static public function CreateTable($tableName, $columns) {
        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);
        $conn->set_charset("utf8mb4");
        if (empty($tableName) || empty($columns) || !is_array($columns)) {
            return "Невірні вхідні параметри";
        }

        // перевірка наявності таблиці
        if(self::checkIfTableExists($tableName)) {
            return 0;
        }

        // Початок складання SQL запиту
        $sql = "CREATE TABLE " . $tableName . " (";
        $sql .= "ID INT NOT NULL AUTO_INCREMENT, "; // Додавання колонки ID

        $columnDetails = [];

        // Додавання колонок у SQL запит
        foreach ($columns as $name => $type) {
            $columnDetails[] = "$name $type";
        }

        $sql .= implode(", ", $columnDetails);
        $sql .= ", PRIMARY KEY (ID)"; // Додавання PRIMARY KEY

        // Встановлення кодування UTF-8 для таблиці
        $sql .= ") DEFAULT CHARSET=utf8mb4;";

        // Виконання запиту
        if ($conn->query($sql) === FALSE) {
            echo "Error creating table: " . $conn->error;
            return -1;
        }

        if ($conn) {
            mysqli_close($conn);
        }

        return 0;
    }


    static public function DropTable($tableName) {
        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);

        if ($conn->connect_error) {
            die("Помилка з'єднання: " . $conn->connect_error);
        }

        $sql = "DROP TABLE IF EXISTS $tableName";

        if ($conn->query($sql) === TRUE) {
            echo "Таблиця $tableName видалена успішно";
        } else {
            echo "Помилка при видаленні таблиці: " . $conn->error;
        }

        $conn->close();
    }

    static public function Select($tableName, $columns = '*', $conditions = '', $orderBy = '', $groupBy = '') {
        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);

        if ($conn->connect_error) {
            die("Помилка з'єднання: " . $conn->connect_error);
        }

        // Формування SQL запиту
        $sql = "SELECT $columns FROM $tableName";
        if (!empty($conditions)) {
            $sql .= " WHERE $conditions";
        }
        if (!empty($groupBy)) {
            $sql .= " GROUP BY $groupBy";
        }
        if (!empty($orderBy)) {
            $sql .= " ORDER BY $orderBy";
        }

        $stmt = $conn->prepare($sql);
        // Передача параметрів у bind_param в залежності від умов
        $stmt->execute();
        $result = $stmt->get_result();

        $data = [];
        while($row = $result->fetch_assoc()) {
            $data[] = $row;
        }

        $stmt->close();
        $conn->close();

        return $data;
    }

    static public function Insert($tableName, $data) {
        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);

        if ($conn->connect_error) {
            die("Помилка з'єднання: " . $conn->connect_error);
        }

        $columns = implode(", ", array_keys($data));
        $placeholders = implode(", ", array_fill(0, count($data), '?'));

        $sql = "INSERT INTO $tableName ($columns) VALUES ($placeholders)";

        $stmt = $conn->prepare($sql);
        $types = str_repeat('s', count($data));
        $stmt->bind_param($types, ...array_values($data));

        if ($stmt->execute()) {
            // Запис успішно створено
        } else {
            // Помилка при створенні запису
        }

        $stmt->close();
        $conn->close();
    }

    static public function Delete($tableName, $condition) {
        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);

        if ($conn->connect_error) {
            die("Помилка з'єднання: " . $conn->connect_error);
        }

        $sql = "DELETE FROM $tableName WHERE $condition";

        $stmt = $conn->prepare($sql);

        if ($stmt->execute()) {
            // Запис видалено успішно
        } else {
            // Помилка при видаленні запису
        }

        $stmt->close();
        $conn->close();
    }


    static public function Update($tableName, $data, $condition) {
        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);

        if ($conn->connect_error) {
            die("Помилка з'єднання: " . $conn->connect_error);
        }

        $updateParts = [];
        foreach ($data as $key => $value) {
            $updateParts[] = "$key = ?";
        }
        $updateStr = implode(", ", $updateParts);

        $sql = "UPDATE $tableName SET $updateStr WHERE $condition";

        $stmt = $conn->prepare($sql);
        $types = str_repeat('s', count($data));
        $stmt->bind_param($types, ...array_values($data));

        if ($stmt->execute()) {
            // Запис оновлено успішно
        } else {
            // Помилка при оновленні запису
        }

        $stmt->close();
        $conn->close();
    }

    static public function DropAllTable() {
        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);

        // Перевірка з'єднання
        if ($conn->connect_error) {
            die("Помилка з'єднання: " . $conn->connect_error);
        }

        // Отримання списку всіх таблиць
        $sql = "SHOW TABLES";
        $result = $conn->query($sql);

        if ($result) {
            while ($row = $result->fetch_array()) {
                $table = $row[0];
                // Видалення кожної таблиці
                $dropQuery = "DROP TABLE $table";
                if ($conn->query($dropQuery) === TRUE) {
                    //echo "Таблиця $table видалена успішно<br>";
                } else {
                    //echo "Помилка при видаленні таблиці $table: " . $conn->error . "<br>";
                }
            }
        } else {
            echo "Помилка при отриманні списку таблиць: " . $conn->error;
        }

            // Закриття з'єднання
        $conn->close();
    }
}
