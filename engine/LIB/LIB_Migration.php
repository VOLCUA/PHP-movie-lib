<?php

namespace LIB;

class LIB_Migration
{
    static public function CheckMigration()
    {
        if(LIB_SQL::checkIfTableExists(tableName: 'Migration')==false)
        {
            self::Migration();
        }
        return 0;
    }

    static public function Migration()
    {
        LIB_SQL::CreateTable('Migration', ["Name" => "VARCHAR(250) NOT NULL"]);

        // Fetch migrated file names from the database and store them in an array
        $Migrated_files = array_map(function($entry) {
            return $entry['Name'];
        }, LIB_SQL::Select('Migration'));

        $dir = "data/Migration";
        // Get all files and folders in the directory
        $allFiles = scandir($dir);

        // Iterate over all items
        foreach ($allFiles as $file) {
            // Full path to the file
            $fullPath = $dir . '/' . $file;

            // Check if the item is a file and not already migrated
            if (is_file($fullPath) && !in_array($file, $Migrated_files)) {
                include $fullPath;
                LIB_SQL::Insert('Migration', ['Name' => $file]);
            }
        }
    }


    static public function Migration_reset()
    {
        LIB_SQL::DropAllTable();
        self::Migration();
    }
}