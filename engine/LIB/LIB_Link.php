<?php

namespace LIB;

class LIB_Link
{
    static public function getInputURLArray()
    {
        // Get the URL request
        $currentURL = $_SERVER['REQUEST_URI'];

        // Split the URL into path and query parameters
        $parts = explode('?', $currentURL);
        $path = $parts[0];
        $getQuery = $parts[1] ?? '';

        // Remove the trailing slash if present in the path
        $path = rtrim($path, '/');

        // Initialize an array to store sanitized query parameters
        $sanitizedGetParams = [];

        // Split and sanitize query parameters
        if ($getQuery) {
            $queryPairs = explode('&', $getQuery);
            foreach ($queryPairs as $queryPair) {
                list($key, $value) = explode('=', $queryPair);
                // Sanitize key and value
                $sanitizedKey = filter_var($key, FILTER_SANITIZE_SPECIAL_CHARS);
                $sanitizedValue = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
                // Add to the sanitized array
                $sanitizedGetParams[$sanitizedKey] = $sanitizedValue;
            }
        }

        // Sanitize POST data
        $sanitizedPostData = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);

        // Initialize arrays for DELETE and PUT data
        $sanitizedDeleteData = [];
        $sanitizedPutData = [];

        if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
            // Sanitize DELETE data
            $rawDeleteData = file_get_contents("php://input");
            parse_str($rawDeleteData, $deleteData);
            foreach ($deleteData as $key => $value) {
                $sanitizedDeleteData[$key] = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        } elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
            // Sanitize PUT data
            $rawPutData = file_get_contents("php://input");
            parse_str($rawPutData, $putData);
            foreach ($putData as $key => $value) {
                $sanitizedPutData[$key] = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }

        // Prepare the result as an array
        $result = [
            'link' => explode('/', $path),
            'method' => $_SERVER['REQUEST_METHOD'],
            'get' => $sanitizedGetParams,
            'post' => $sanitizedPostData,
            'delete' => $sanitizedDeleteData,
            'put' => $sanitizedPutData,
        ];

        return $result;
    }
    static public function Page404()
    {
        LIB_Link::Load_Page(View_file: '404');
    }
    static public function Load_Page($View_file,$View_title="",$View_input_array=[],$CheckLogin=false)
    {
        $_VIA=$View_input_array;



        if($CheckLogin)
        {
            if(USER_IS_LOGINED)
            {
                include "data/View/layouts/Header.php";
                include "data/View/".$View_file.".php";
                include "data/View/layouts/Footer.php";
            }
            else
            {
                \LIB\LIB_Link::Load_Page(View_file: 'user_login',View_title: "Увійти");
            }
        }
        else
        {
            include "data/View/layouts/Header.php";
            include "data/View/".$View_file.".php";
            include "data/View/layouts/Footer.php";
        }

    }
}
