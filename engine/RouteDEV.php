<?php

function DEV_redirect()
{
    header("Location: ".PROJECT_URL_FULL."dev/?DEV=".DEV_KEY);
    die();
}


if(count(INPUT_URL_ARRAY['link'])>2)
{
    switch (INPUT_URL_ARRAY['link'][2])
    {
        case "migration":
            \LIB\LIB_Migration::Migration();
            DEV_redirect();
            break;
        case "migration_reset":
            \LIB\LIB_Migration::Migration_reset();
            DEV_redirect();
            break;
    }
}

include 'DEV/DEV_Page.php';