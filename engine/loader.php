<?php



//Check config file exist and connect
use LIB\LIB_DEV;
use LIB\LIB_Link;
use LIB\LIB_Migration;
use LIB\LIB_SQL;


include "config/config.php";

//TODO
//Доробити та зробити адекватний захист
//if (isset($_SERVER['HTTP_REFERER'])) {
//    $referer_host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
//    if ($referer_host !== PROJECT_URL_FULL) {
//        header('HTTP/1.1 403 Forbidden');
//        exit;
//    }
//}

//include standard LIB
include "LIB/LIB_Link.php";
include "LIB/LIB_SQL.php";
include "LIB/LIB_DEV.php";
include "LIB/LIB_Migration.php";

//load input param
define('INPUT_URL_ARRAY',LIB_Link::getInputURLArray());

//Status site Off / On
LIB_DEV::Site_status();

//Check connect
LIB_SQL::CheckConnect();

//Check Migration
LIB_Migration::CheckMigration();

//Load all model and controller
$dir="data/Model";
$allFiles = scandir($dir);
foreach ($allFiles as $file) {
    $fullPath = $dir . '/' . $file;
    if (is_file($fullPath)) {
            include $fullPath;
    }
}

$dir="data/Controller";
$allFiles = scandir($dir);
foreach ($allFiles as $file) {
    $fullPath = $dir . '/' . $file;
    if (is_file($fullPath)) {
        include $fullPath;
    }
}

//Login user
session_start();
$user_is_logined=false;
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    $user_is_logined=true;
}
define('USER_IS_LOGINED',$user_is_logined);

header('Content-Type: text/html; charset=utf-8');