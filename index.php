<?php
//Load System
include "engine/loader.php";

if(count(INPUT_URL_ARRAY['link'])>1)
{
    if(strtolower(INPUT_URL_ARRAY['link'][1])=='api')
    {
        include "data/RouteApi.php";
    }
    else if(strtolower(INPUT_URL_ARRAY['link'][1])=='cron')
    {

        if(isset($_GET['DEV'])&&(DEV_KEY==$_GET['DEV']))
        {

            include "data/RouteCron.php";
        }
    }
    else if(strtolower(INPUT_URL_ARRAY['link'][1])=='dev')
    {
        if(\LIB\LIB_DEV::IsDev()){include "engine/RouteDEV.php";}
        else
        {echo "Ти не розробник!!!!";}
    }
    else
    {include "data/Route.php";}
}
else
{include "data/Route.php";}