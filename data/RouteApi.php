<?php


use Controller\MovieController;
use Controller\UserController;

switch(strtolower(INPUT_URL_ARRAY['link'][2]))
{

    case "user_registration":
        UserController::user_registration();
        break;

    case "user_login":
        UserController::user_login();
        break;

    case "movie_import":
        MovieController::movie_import();
        break;

    case "movie_create":
        MovieController::movie_create();
        break;

    case "movie_update":
        MovieController::movie_update();
        break;

    case "movie_delete":
        MovieController::movie_delete();
        break;

    case "movie_get_list_filter":
        MovieController::movie_get_list_filter();
        break;

    default:
        \LIB\LIB_Link::Page404();
        break;

}