<?php

use Controller\MovieController;
use Controller\UserController;
use LIB\LIB_Link;

if(count(INPUT_URL_ARRAY['link'])>1){
switch(strtolower(INPUT_URL_ARRAY['link'][1]))
    {
    case "movie_create":
        MovieController::page_movie_create();
        break;
    case "movie_edit":
        MovieController::page_movie_edit();
        break;
    case "movie_import":
        MovieController::page_movie_import();
        break;
    case "movie_page":
        MovieController::page_movie_page();
        break;

    case "user_registration":
        UserController::page_user_registration();
        break;
    case "user_login":
        UserController::page_user_login();
        break;
    case "user_profile":
        UserController::page_user_profile();
        break;
    case "user_logout":
        UserController::page_user_logout();
        break;
    default:
        LIB_Link::Page404();
        break;
    }
}
else
{
    LIB_Link::Load_Page(View_file: 'home');
}

