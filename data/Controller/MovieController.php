<?php

namespace Controller;

use LIB\LIB_Link;
use LIB\LIB_SQL;
use Model\Movie;

class MovieController
{
    //region movie_page
    static public function page_movie_create()
    {
        LIB_Link::Load_Page(View_file: 'movie_create',View_title: "Створити фільм",CheckLogin: true);
    }

    static public function page_movie_edit()
    {
        if((count(INPUT_URL_ARRAY['link'])>2))
        {
            $movie_id=INPUT_URL_ARRAY['link'][2];
            define('MOVIE',Movie::GetById($movie_id));
            if(MOVIE!=null) {
                LIB_Link::Load_Page(View_file: 'movie_edit', View_title: "Редагування фільм", CheckLogin: true);
            }
            else
            {
                LIB_Link::Page404();
            }
        }
        else
        {
            LIB_Link::Page404();
        }
    }

    static public function page_movie_import()
    {
        LIB_Link::Load_Page(View_file: 'movie_import',View_title: "Імпортувати",CheckLogin: true);
    }

    static public function page_movie_page()
    {
        LIB_Link::Load_Page(View_file: 'movie_page');
    }
    //endregion movie_page


    static public function movie_import()
    {
        $file = $_FILES['fileUpload'];

        if ($file['error'] === UPLOAD_ERR_OK) {

            $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);
            if ($fileExtension !== 'txt') {
                echo "Дозволені лише файли у форматі .txt";
                return;
            }

            if (empty(file_get_contents($file['tmp_name']))) {
                echo "Файл пустий.";
                return;
            }


            $fileContent = file_get_contents($file['tmp_name']);
            $movies = explode("\n\n", trim($fileContent));
            foreach ($movies as $movie) {
                $lines = explode("\n", $movie);
                if(
                    str_contains($lines[0], 'Title: ')&&
                    str_contains($lines[1], 'Release Year: ')&&
                    str_contains($lines[2], 'Format: ')&&
                    str_contains($lines[3], 'Stars: ')
                ){
                $title = str_replace('Title: ', '', $lines[0]);
                $releaseYear = str_replace('Release Year: ', '', $lines[1]);
                $format = str_replace('Format: ', '', $lines[2]);
                $stars = str_replace('Stars: ', '', $lines[3]);

                Movie::Create($title, $releaseYear, $format, $stars);
                }
                else
                {
                    echo "<br><br>Помилка Імпорту фільму<br><br>" .str_replace("\n",'<br>',$movie);
                }
            }

            echo "Транзакція пройшла вдало";

        } else {
            echo "Помилка при завантаженні файлу.";
        }
    }

    static public function movie_create()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST['movie_name']) && isset($_POST['movie_year']) && isset($_POST['movie_format']) && isset($_POST['movie_actors'])) {

                $resultate=Movie::Create(Name: $_POST['movie_name'],Year: $_POST['movie_year'],Format: $_POST['movie_format'],Actor_list: $_POST['movie_actors']);
                echo json_encode($resultate);
            }
        }
    }

    static public function movie_update()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST['movie_name']) && isset($_POST['movie_year']) && isset($_POST['movie_format']) && isset($_POST['movie_actors']) && isset($_POST['movie_id'])) {

                $resultate=Movie::Update(Name: $_POST['movie_name'],Year: $_POST['movie_year'],Format: $_POST['movie_format'],Actor_list: $_POST['movie_actors'],ID_Movie: $_POST['movie_id']);
                echo json_encode($resultate);
            }
        }
    }

    static public function movie_delete()
    {
        Movie::Delete($_POST['movie_id']);
    }

    static public function movie_get_list_filter()
    {
        echo json_encode(Movie::GetListFilter());
    }


}