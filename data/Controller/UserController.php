<?php

namespace Controller;

use LIB\LIB_Link;
use Model\User;

class UserController
{

    //region user_page
    static public function page_user_registration()
    {
        LIB_Link::Load_Page(View_file: 'user_registration',View_title: "Зарегіструватися");
    }

    static public function page_user_login()
    {
        LIB_Link::Load_Page(View_file: 'user_login',View_title: "Увійти");
    }

    static public function page_user_profile()
    {
        LIB_Link::Load_Page(View_file: 'user_profile',View_title: "Профіль");
    }

    static public function page_user_logout()
    {
        User::logout();
    }
    //endregion user_page

    //region user_api
    static public function user_login()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST['LOGIN']) && isset($_POST['PASS'])) {
                $LOGIN=$_POST['LOGIN'];
                $PASS=$_POST['PASS'];

                User::login(LOGIN: $LOGIN,PASS: $PASS,ShowMessage: true);
            }
            else {
                echo "Username або Password відсутні в запиті.";
            }
        }

    }

    static public function user_registration()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST['LOGIN']) && isset($_POST['PASS'])) {
                $LOGIN=$_POST['LOGIN'];
                $PASS=$_POST['PASS'];

                User::register(LOGIN: $LOGIN,PASS: $PASS,ShowMessage: true);
            }
            else {
                echo "Username або Password відсутні в запиті.";
            }
        }
    }
    //endregion user_api

}