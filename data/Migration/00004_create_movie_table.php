<?php

$tableName = "Movie";
$columns = [
    "Name" => "VARCHAR(500) NOT NULL",
    "Year" => "INT(32) NOT NULL",
    "Format" => "VARCHAR(100)",
    "Actor_list" => "VARCHAR(600)",
];

\LIB\LIB_SQL::CreateTable(tableName: $tableName,columns: $columns);