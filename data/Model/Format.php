<?php

namespace Model;

use LIB\LIB_SQL;

class Format
{

    static public function Create($NameFormat)
    {
        if(count(LIB_SQL::Select(tableName: 'Movie_format', conditions: "Name='$NameFormat'"))==0) {
            $NameFormat = trim($NameFormat);
            LIB_SQL::Insert('Movie_format', ['Name' => $NameFormat]);
        }
    }

    static public function GetList()
    {
        return LIB_SQL::Select('Movie_format');
    }
}