<?php

namespace Model;

use LIB\LIB_Link;

class User
{
    static public function register($LOGIN, $PASS, $ShowMessage = false) {
        $resultate = false;
        $db = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);

        if ($db->connect_error) {
            if ($ShowMessage) echo "Помилка з'єднання: " . $db->connect_error;
            return false;
        }

        // Перевірка наявності логіну в базі даних
        $stmt = $db->prepare("SELECT LOGIN FROM User WHERE LOGIN = ?");
        $stmt->bind_param("s", $LOGIN);
        $stmt->execute();
        $checkResult = $stmt->get_result();

        if ($checkResult && $checkResult->num_rows > 0) {
            if ($ShowMessage) echo "Логін вже існує";
            $stmt->close();
            return false;
        }
        $stmt->close();

        // Якщо логін унікальний, продовжуємо реєстрацію
        $password = password_hash($PASS . SECURITY_KEY, PASSWORD_DEFAULT);
        $insertStmt = $db->prepare("INSERT INTO User (LOGIN, PASS) VALUES (?, ?)");
        $insertStmt->bind_param("ss", $LOGIN, $password);

        if ($insertStmt->execute()) {
            if ($ShowMessage) echo "REG_DONE";
            $resultate = true;
        } else {
            if ($ShowMessage) echo "Error: " . $insertStmt->error;
            $resultate = false;
        }

        $insertStmt->close();
        $db->close();
        return $resultate;
    }



    static public function login($LOGIN, $PASS, $ShowMessage = false) {
        $db = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);

        if ($db->connect_error) {
            if ($ShowMessage) echo "Помилка з'єднання: " . $db->connect_error;
            return;
        }

        // Підготовка запиту
        $stmt = $db->prepare("SELECT id, PASS FROM User WHERE LOGIN = ?");
        $stmt->bind_param("s", $LOGIN);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $password = $PASS . SECURITY_KEY;
            if (password_verify($password, $row['PASS'])) {
                $_SESSION['loggedin'] = true;
                $_SESSION['username'] = $LOGIN;
                $_SESSION['user_id'] = $row['id'];
                if ($ShowMessage) echo "LOGIN_DONE";
            } else {
                if ($ShowMessage) echo "Невірний пароль!";
            }
        } else {
            if ($ShowMessage) echo "Користувача не знайдено!";
        }

        $stmt->close();
        $db->close();
    }

    static public function logout()
    {
//        session_start();
        session_destroy();
        header("Location: ".PROJECT_URL_FULL);
        die();

    }

}