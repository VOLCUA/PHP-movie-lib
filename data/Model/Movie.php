<?php

namespace Model;

use LIB\LIB_SQL;

class Movie
{

    //region CRUD




    static public function GetListFilter() {
        $conn = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, DB_PORT);
       // $conn->set_charset("utf8mb4");

        if ($conn->connect_error) {
            die("Помилка з'єднання: " . $conn->connect_error);
        }

        $conditions = [];
        $orderBy = '';

        if (!empty($_POST['name'])) {
            $name = $conn->real_escape_string($_POST['name']);
            $conditions[] = "Name LIKE '%$name%'";
        }
        if (!empty($_POST['year_min'])) {
            $year_min = intval($_POST['year_min']);
            $conditions[] = "Year >= $year_min";
        }
        if (!empty($_POST['year_max'])) {
            $year_max = intval($_POST['year_max']);
            $conditions[] = "Year <= $year_max";
        }
        if (!empty($_POST['format'])) {
            $formatConditions = array_map(function($format) use ($conn) {
                return "'" . $conn->real_escape_string($format) . "'";
            }, $_POST['format']);
            $conditions[] = "Format IN (" . implode(',', $formatConditions) . ")";
        }
        if (!empty($_POST['actor'])) {
            $actorConditions = array_map(function($actor) use ($conn) {
                return "Actor_list LIKE '%" . $conn->real_escape_string(trim($actor)) . "%'";
            }, $_POST['actor']);
            $conditions[] = '(' . implode(' OR ', $actorConditions) . ')';
        }

        if (!empty($_POST['sort'])) {
            $orderBy = $_POST['sort'] === 'Alphabetically' ? 'BINARY Name COLLATE utf8mb4_unicode_ci ASC' : 'ID DESC';
        }

        $conditionString = !empty($conditions) ? implode(' AND ', $conditions) : '1=1';

        $query = "SELECT * FROM Movie WHERE $conditionString ORDER BY $orderBy";
        $result = $conn->query($query);
        $data = $result->fetch_all(MYSQLI_ASSOC);

        if($_POST['sort'] === 'Alphabetically') {
            usort($data,      function($a, $b) {
                $order = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZzАаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЬьЮюЯя';
                $aName = mb_strtolower($a['Name']);
                $bName = mb_strtolower($b['Name']);

                $maxLength = max(strlen($aName), strlen($bName));
                for ($i = 0; $i < $maxLength; $i++) {
                    $posA = strpos($order, $aName[$i]) ?: ord($aName[$i]);
                    $posB = strpos($order, $bName[$i]) ?: ord($bName[$i]);
                    if ($posA != $posB) {
                        return $posA - $posB;
                    }
                }
                return 0;
            });
        }

        $conn->close();
        return $data;
    }



    static public function GetById($id)
    {
        return LIB_SQL::Select( tableName: 'Movie',conditions: 'ID='.$id)[0];
    }

    static public function GetByName($MovieName)
    {
        return LIB_SQL::Select( tableName: 'Movie',conditions: 'Name='.$MovieName)[0];
    }

    static public function GetByConditions($conditions)
    {
        return LIB_SQL::Select( tableName: 'Movie',conditions: $conditions);
    }


    static public function validateMovieData($Name, $Year, $Format, $Actor_list,$isUpdate=false) {
        $currentYear = date("Y");



        if (empty($Name) || !preg_match('/^[a-zA-Z0-9\s.,!?\'-а-яА-Я]+$/', $Name)) {
            return ['success' => false, 'message' => 'Назва фільму містить недопустимі символи або є обов\'язковою до заповнення'];
        }

        $existingMovie = LIB_SQL::Select(tableName: 'Movie', conditions: "Name='$Name'");
        if (!$isUpdate&&(count($existingMovie) > 0)) {

            return ['success' => false, 'message' => 'Фільм з такою назвою вже існує'];
        }

        if (empty($Year) || !is_numeric($Year) || $Year < 1900 || $Year > $currentYear) {
            return ['success' => false, 'message' => 'Невірний рік. Вкажіть рік від 1900 до сьогоднішнього року'];
        }

        if (empty($Format)) {
            return ['success' => false, 'message' => 'Формат обов\'язковий до заповнення'];
        }

        if (empty($Actor_list)) {
            return ['success' => false, 'message' => 'Список акторів обов\'язковий до заповнення'];
        }

        $Actor_array = explode(",", $Actor_list);
        foreach ($Actor_array as $Actor) {
            $Actor = trim($Actor);
            if (empty($Actor) || !preg_match('/^[a-zA-Z0-9\s.,!?\'-а-яА-Я]+$/', $Actor)) {
                return ['success' => false, 'message' => 'Назви акторів містять недопустимі символи'];
            }
        }

        return ['success' => true];
    }

    static public function Create($Name,$Year,$Format,$Actor_list)
    {

        $Name = trim($Name);
        $Format = trim($Format);
        $Actor_list = trim($Actor_list);

        $validationResult = self::validateMovieData($Name, $Year, $Format, $Actor_list);
        if (!$validationResult['success']) {
            return $validationResult;
        }

        if(count(LIB_SQL::Select(tableName: 'Movie', conditions: "Name='$Name'"))==0){

            $Actor_array=explode(",", $Actor_list);
            $Actor_array_db=Actor::GetList();

            $Actor_array_db_name=[];

            foreach ($Actor_array_db as $Actor)
            {
                array_push($Actor_array_db_name,$Actor['Name']);
            }

            foreach ($Actor_array as $Actor){
            if(($Actor!='')&&(!in_array($Actor, $Actor_array_db_name)))
            {
                Actor::Create($Actor);
            }
            }

            LIB_SQL::Insert('Movie',
                ['Name'=>$Name,'Year'=> $Year,'Format'=> $Format,'Actor_list'=>$Actor_list]
            );

            $id=LIB_SQL::Select(tableName: 'Movie',columns: 'id',conditions: "Name='$Name'")[0]['id'];
            return ['success'=>true,'message'=>'All Ok','id'=> $id];

        }
        else
        {
            return ['success'=>false,'message'=>'Повторки не додаємо - дайте іншу назву','id'=> 0];
        }
    }

    static public function Update($Name,$Year,$Format,$Actor_list,$ID_Movie)
    {

        $Name = trim($Name);
        $Format = trim($Format);
        $Actor_list = trim($Actor_list);

        $validationResult = self::validateMovieData($Name, $Year, $Format, $Actor_list,true);
        if (!$validationResult['success']) {
            return $validationResult;
        }

        $Movie = self::GetById($ID_Movie);

        if(count($Movie)!=0){
            LIB_SQL::Update('Movie',
                ['Name'=>$Name,
                    'Year'=> $Year,
                    'Format'=> $Format,
                    'Actor_list'=>$Actor_list
                ],
                'ID='.$ID_Movie
            );
            Actor::UpdateListActorMovie(ActorListPre: $Movie['Actor_list'],ActorListNew: $Actor_list);

            return ['success'=>true,'message'=>'All Ok','id'=> $ID_Movie];
        }
        else
        {
            return ['success'=>false,'message'=>'Такого ID не існує','id'=> 0];
        }
    }

    static public function Delete($ID)
    {

        $Actor_list_old = self::GetById($ID)['Actor_list'];

        LIB_SQL::Delete('Movie',"ID=".$ID);

        Actor::UpdateListActorMovie($Actor_list_old,'');
    }

}