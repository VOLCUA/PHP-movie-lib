<?php

namespace Model;

use LIB\LIB_SQL;

class Actor
{
    static public function GetList()
    {
        return LIB_SQL::Select('Movie_actor');
    }

    static public function Create($NameActor)
    {
        $NameActor = trim($NameActor);
        if(count(LIB_SQL::Select(tableName: 'Movie_actor', conditions: "Name='$NameActor'"))==0){
            LIB_SQL::Insert('Movie_actor',['Name'=>$NameActor]);
        }
    }

    static public function DeleteByName($NameActor)
    {
        $ActorExistInMovieTable = false;

        $Movies = Movie::GetByConditions("(Actor_list like '%$NameActor,%') or (Actor_list like '%,$NameActor,%') or (Actor_list like '%, $NameActor%')");

        //echo count($Movies)."-$NameActor<br>";
        if(count($Movies)==0){

            $ActorExistInMovieTable = false;
        }
        else{
        foreach ($Movies as $Movie)
        {
            $ActorsMovie = $Movie['Actor_list'];
            $ActorsMovie_array=explode(",", $ActorsMovie);

            foreach ($ActorsMovie_array as $ActorMovie)
            {
                if(trim($ActorMovie)==trim($NameActor))
                {
                    $ActorExistInMovieTable = true;
                }
            }
        }
        }

        if(!$ActorExistInMovieTable)
        LIB_SQL::Delete('Movie_actor',"Name='".$NameActor."'");
    }

    static public function Clear()
    {
        $Movie_actors =LIB_SQL::Select('Movie_actor');
        foreach ($Movie_actors as $Movie_actor)
        {
            self::DeleteByName($Movie_actor['Name']);
        }

        echo "Всі зайві актори були видалені";
    }


    static public function UpdateListActorMovie($ActorListPre,$ActorListNew)
    {

        $ActorListPre_Array=explode(",", $ActorListPre);
        $ActorListNew_Array=explode(",", $ActorListNew);

        $ActorList_Delete = array_diff($ActorListPre_Array, $ActorListNew_Array);
        $ActorList_Create = array_diff($ActorListNew_Array, $ActorListPre_Array);

        foreach ($ActorList_Delete as $ActorDel)
        {
            self::DeleteByName($ActorDel);
        }

        foreach ($ActorList_Create as $ActorNew)
        {
            self::Create($ActorNew);
        }
    }
}