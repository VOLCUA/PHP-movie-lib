<style>
    .movie_filter
    {
        background: gray;
        border-radius: 20px;
    }
</style>

<style>
    .bootstrap-tagsinput {
        display: flex !important;
        flex-wrap: wrap;
        align-items: center;
        gap: 6px;
        width: 550px;
    }

    .bootstrap-tagsinput .tag {
        background: #90b695;
        padding: 4px 10px;
        font-size: 14px;
        border-radius: 4px;
    }

    .movie_filter {
        background-color: #86dca9;
        border-radius: 8px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        padding: 20px;
        margin-top: 20px;
    }

    .movie_filter h2, .movie_filter h3 {
        color: #333;
        margin-bottom: 15px;
    }

    .movie_filter .custom-select, .movie_filter .form-control {
        margin-bottom: 15px;
    }

    .movie_filter .form-check {
        margin-bottom: 10px;
    }

    .movie_filter .form-check-input {
        margin-right: 5px;
    }

    .movie_filter .tags-input {
        margin-bottom: 15px;
    }

    .movie_filter .btn-primary {
        background-color: #007bff;
        border-color: #007bff;
    }

    .movie_filter .btn-primary:hover {
        background-color: #0056b3;
    }

    #filter-actor-list {
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        margin-top: 10px;
    }

    .Actor_Item {
        padding: 5px 10px;
        border-bottom: 1px solid #eee;
        cursor: pointer;
    }

    .Actor_Item:hover {
        background-color: #f1f1f1;
    }


</style>

<div class="col-3 movie_filter" onclick="home_filter();">
    <h2>Фільтр</h2>
    <h3>Сортування</h3>
    <select class="custom-select" id="filter-movie-sort" >
        <option value="Default">По додаванню</option>
        <option value="Alphabetically">По алфавітному порядку</option>
    </select>

    <h3>Назва Фільму</h3>
    <input oninput="home_filter();" type="text" class="form-control" placeholder="Назва фільму" id="filter-movie-name">

    <h3>Рік випуску</h3>
    <div class="d-flex">
        <input oninput="home_filter();" type="number" class="form-control mr-2" placeholder="Від" id="filter-movie-year-min">
        <input oninput="home_filter();" type="number" class="form-control" placeholder="До" id="filter-movie-year-max">
    </div>

    <h3>Формат</h3>
    <?php
    foreach (\Model\Format::GetList() as $Format)
    {
        ?>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="<?php echo $Format['Name'];?>" id="format<?php echo $Format['ID'];?>">
            <label class="form-check-label" for="format<?php echo $Format['ID'];?>">
                <?php echo $Format['Name'];?>
            </label>
        </div>
        <?php
    }
    ?>
    <h3>Список акторів</h3>
    <input type="text" class="form-control tags-input" name="Actor" id="movie_actors"/>
    <input type="text" oninput="filterActors();" class="form-control" placeholder="Пошук акторів" id="filter-actor-search">
    <div class="text-center" id="filter-actor-list" style="height: 200px; overflow-x: auto;">
        <?php foreach (\Model\Actor::GetList() as $Actor) { ?>
            <div class="Actor_Item" data-actor-name="<?php echo htmlspecialchars($Actor['Name']); ?>">
                <?php echo htmlspecialchars($Actor['Name']); ?>
            </div>
        <?php } ?>
    </div>

    <button class="btn btn-primary w-100 mt-4" onclick="home_filter();">Пошук</button>
</div>

<div class="col-1">
</div>
<div class="col-8 " id="movie_list">
    <div class="row" id="movie_list_content" style="overflow: auto;height: 900px">
        <div class="col-12">
            <h1>Title (Year)[Format]</h1>
            <p>
                Брали участь:<br>
                Actor
            </p>
        </div>
    </div>
</div>

<script>

    var home_filter= function()
    {
        // Get type sort
        var sort = document.getElementById('filter-movie-sort').value;
        // Get name movie
        var name = document.getElementById('filter-movie-name').value;
        // Get year
        var year_min = document.getElementById('filter-movie-year-min').value;
        var year_max = document.getElementById('filter-movie-year-max').value;

        // Get Format
        var format_CB=document.getElementsByClassName('form-check-input');
        var format=[];
        for(var i=0;format_CB.length>i;i++)
        {
            if(format_CB[i].checked)
            {
                format.push(format_CB[i].value);
            }
        }

        // Get actor selected
        var actor =$('#movie_actors').val();

        $.post("/api/movie_get_list_filter", {
            sort: sort,
            name: name,
            year_min: year_min,
            year_max: year_max,
            'format[]': format,
            'actor[]': actor.split(',')
        }, "json")
            .done(function(data) {
                try {
                    // Якщо відповідь сервера вже є у форматі JSON, не потрібно використовувати JSON.parse()
                    const obj = JSON.parse(data);
                    $('#movie_list_content').empty(); // Очищення вмісту контейнера перед додаванням нових елементів
                    obj.forEach(function(movie) {
                        // Створення рядка HTML для кожного фільму
                        var movieHtml =
                            '<div class="col-12">' +
                            '<h1>' + movie.Name + ' (' + movie.Year + ') [' + movie.Format + ']</h1>' +
                            '<p>Брали участь:<br>' + movie.Actor_list + '</p>' +
                            '<a href="/movie_edit/'+movie.ID+'"><button class="btn btn-primary"> Редагувати </button></a>'
                            '</div>';
                        // Додавання HTML до DOM
                        $('#movie_list_content').append(movieHtml);
                    });
                } catch (e) {
                    $('#error-message').html(data);
                }
            })

    }


    $(document).ready(function() {
        $('.Actor_Item').click(function() {
            var actorName = $(this).data('actor-name');
            ActorAddToFilter(actorName);
        });
    });

    function ActorAddToFilter(actorName) {
        $('#movie_actors').tagsinput('add', actorName)
    }

    function ActorFilter(actorName) {
        $('#movie_actors').val()
    }

    $(document).ready(function() {
        $('.tags-input').tagsinput();
    });


    function filterActors() {
        var filter = $('#filter-actor-search').val().toUpperCase();

        $('#filter-actor-list .Actor_Item').each(function() {
            var actorName = $(this).data('actor-name').toUpperCase();
            if (actorName.indexOf(filter) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }

    home_filter();
</script>