
<style>
    .bootstrap-tagsinput {
        display: flex !important;
        flex-wrap: wrap;
        align-items: center;
        gap: 6px;
        width: 550px;
    }

    .bootstrap-tagsinput .tag {
        background: #90b695;
        padding: 4px 10px;
        font-size: 14px;
        border-radius: 4px;
    }

</style>

<div class="col-12">

    <div class="form-group">
        <label for="sehir">Назва фільму</label>
        <input type="text" class="form-control mr-2" placeholder="Назва фільму" id="movie_name">
    </div>

    <div class="form-group">
        <label for="sehir">Рік випуску</label>
        <input type="number" class="form-control mr-2" placeholder="Рік" id="movie_year">
    </div>

    <div class="form-group">
        <label for="sehir">Формат</label>
        <br>
        <div >
            <?php
            foreach (\Model\Format::GetList() as $Format)
            {
                ?>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="<?php echo $Format['Name'];?>" id="format<?php echo $Format['ID'];?>">
                    <label class="form-check-label" for="format<?php echo $Format['ID'];?>">
                        <?php echo $Format['Name'];?>
                    </label>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <div class="form-group">
        <label for="sehir">Список акторів</label>
        <input type="text" class="form-control tags-input" name="Actor" id="movie_actors"/>
    </div>

</div>

<div class="col-sm-12" id="error-message" style="color: Red">
    <!-- Тут буде відображатися повідомлення про помилку -->
</div>

<script>
    $(document).ready(function() {
        $('.tags-input').tagsinput();
    });
</script>