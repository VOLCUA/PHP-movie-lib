

<div class="row g-3 col-12" id="authForm">
    <div class="col-sm-6">
        <label for="login" class="form-label">LOGIN</label>
        <input type="text" class="form-control" id="login" placeholder="" required="">
    </div>

    <div class="col-sm-6">
        <label for="password" class="form-label">PASS</label>
        <input type="password" class="form-control" id="password" placeholder="" required="">
    </div>

    <div class="col-sm-12" id="error-message" style="color: Red">
        <!-- Тут буде відображатися повідомлення про помилку -->
    </div>

    <div class="col-sm-12">
        <button type="button" class="btn btn-success w-100" onclick="submitForm()">
            Зарегіструватися
        </button>
    </div>
</div>


<script>
    function submitForm() {
        const login = document.getElementById('login').value;
        const password = document.getElementById('password').value;
        const errorMessage = document.getElementById('error-message');

        // Перевірка валідності даних
        if (!login || !password) {
            errorMessage.textContent = 'Будь ласка, заповніть всі поля!';
            return;
        }

        $.post( "/api/user_registration",{LOGIN: login,PASS: password}, "json")
            .done(function( data ) {
                if(data=='REG_DONE')
                {
                    alert( "Ваш Профіль заругістрований. просимо авторизуватися" );
                    location.href = "<?php echo PROJECT_URL_FULL?>user_login/";
                }
                else
                {
                    errorMessage.innerHTML=data;
                }
            });
    }


</script>