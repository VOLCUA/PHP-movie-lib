
<?php
include "component/movie_form.php";
?>

<div class="col-sm-12">
    <button type="button" class="btn btn-success w-100" onclick="submitForm()">
        Створити
    </button>
</div>

<script>
    function submitForm() {
        const movie_name = document.getElementById('movie_name').value;
        const movie_year = document.getElementById('movie_year').value;
        const movie_actors = document.getElementById('movie_actors').value;
        const errorMessage = document.getElementById('error-message');

        var format_CB=document.getElementsByClassName('form-check-input');
        var movie_format=[];
        for(var i=0;format_CB.length>i;i++)
        {
            if(format_CB[i].checked)
            {

                movie_format.push(format_CB[i].value);
            }
        }

        var movie_format=movie_format.join(',');
        console.log(movie_format);
        // Перевірка валідності даних
        if (!movie_name || !movie_year || !movie_actors || !movie_actors) {
            errorMessage.textContent = 'Будь ласка, заповніть всі поля!';
            return;
        }



        $.post( "/api/movie_create",{movie_name: movie_name,movie_year: movie_year,movie_format:movie_format,movie_actors: movie_actors}, "json")
            .done(function( data ) {
                try
                {
                    const obj = JSON.parse(data)
                    if(obj.success){
                        alert('Фільм успішно створений');
                    location.href = "<?php echo PROJECT_URL_FULL?>movie_edit/"+obj.id;
                    }
                    else
                    {
                        errorMessage.innerHTML=obj.message;
                    }
                }
                catch (e) {
                    errorMessage.innerHTML=data;
                }
            });
    }
</script>