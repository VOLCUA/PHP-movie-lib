
<?php
include "component/movie_form.php";
?>

<input type="hidden" id="movie_id" value="">

<div class="col-sm-12">
    <button type="button" class="btn btn-primary w-100" onclick="submitForm()">
        Редагувати
    </button>

    <button type="button" class="btn btn-danger w-100" onclick="submitFormDEL()">
        Видалити
    </button>
</div>

<script>

    var Init = function ()
    {
        var JSON_movie = '<?php echo json_encode(MOVIE);?>';
        var movie = JSON.parse(JSON_movie);
        console.dir(movie);

        document.getElementById('movie_id').value=movie.ID;
        document.getElementById('movie_name').value=movie.Name;
        document.getElementById('movie_year').value=movie.Year;
        var format =movie.Format;
        var formatCB = document.getElementsByClassName('form-check-input');

        for(var i=0;formatCB.length>i;i++)
        {
            if(format.includes(formatCB[i].value))
            {
                formatCB[i].checked=true;
            }
        }

        $("#movie_actors").val(movie.Actor_list);
    }

    Init();

    function submitForm() {
        const movie_id = document.getElementById('movie_id').value;
        const movie_name = document.getElementById('movie_name').value;
        const movie_year = document.getElementById('movie_year').value;
        const movie_actors = document.getElementById('movie_actors').value;
        const errorMessage = document.getElementById('error-message');

        var format_CB=document.getElementsByClassName('form-check-input');
        var movie_format=[];
        for(var i=0;format_CB.length>i;i++)
        {
            if(format_CB[i].checked)
            {
                movie_format.push(format_CB[i].value);
            }
        }

        var movie_format=movie_format.join(',');
        console.log(movie_format);
        // Перевірка валідності даних
        if (!movie_name || !movie_year || !movie_actors || !movie_id) {
            errorMessage.textContent = 'Будь ласка, заповніть всі поля!';
            return;
        }



        $.post( "/api/movie_update",
            {movie_name: movie_name,
                movie_year: movie_year,
                movie_format:movie_format,
                movie_actors: movie_actors,
                movie_id: movie_id
            }, "json")
            .done(function( data ) {
                try
                {
                    const obj = JSON.parse(data)
                    if(obj.success){
                        alert('Фільм успішно Оновлено');
                        errorMessage.innerHTML="";
                    }
                    else
                    {
                        errorMessage.innerHTML=obj.message;
                    }
                }
                catch (e) {
                    errorMessage.innerHTML=data;
                }
            });
    }



    function submitFormDEL()
    {
        if (confirm('Ви точно хочете видалити цей фільм?')){
        $.post( "/api/movie_delete",
            {movie_id: document.getElementById('movie_id').value}, "json")
            .done(function( data ) {
                alert("Фільм видалений");
                location.href = "<?php echo PROJECT_URL_FULL?>";
            });
        }
    }
</script>