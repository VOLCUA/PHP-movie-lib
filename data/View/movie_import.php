<div class="container mt-5">
    <form id="uploadForm" enctype="multipart/form-data">
        <div class="form-group">
            <label for="fileUpload">Виберіть файл для завантаження:</label>
            <input type="file" class="form-control-file" id="fileUpload" name="fileUpload">
        </div>
        <button type="button" class="btn btn-primary" onclick="submitForm()">Завантажити</button>
    </form>
    <div id="result"></div>
</div>


<script>
    function submitForm() {
        const form = document.getElementById('uploadForm');
        const fileInput = document.getElementById('fileUpload');
        const resultDiv = document.getElementById('result');

        const formData = new FormData();
        if (fileInput.files.length > 0) {
            formData.append('fileUpload', fileInput.files[0]);

            fetch('/api/movie_import', {
                method: 'POST',
                body: formData
            })
                .then(response => response.text())
                .then(data => {

                    resultDiv.innerHTML = data;
                })
                .catch(error => {
                    console.error('Помилка:', error);
                    resultDiv.innerHTML = 'Помилка при завантаженні файлу';
                });
        } else {
            resultDiv.innerHTML = 'Будь ласка, виберіть файл для завантаження';
        }
    }
</script>